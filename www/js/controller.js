serialize = function(obj){
	var str = [];
    for(var p in obj)
    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    return str.join("&");
}

getDate = function(fulldate){
	var mydate = new Date(fulldate);
	return mydate.toString('yyyy-MM-dd');
}
var popup;
var loading;

showLoading = function($ionicLoading)
{
	loading = $ionicLoading.show({
		template:'Mohon Tunggu...'
	});

}

hideLoading = function($ionicLoading)
{
	$ionicLoading.hide();
}
showAlert = function(title,dataString,$ionicPopup,$scope,withbutton){
		var data = {};
		data.template = '<center>'+dataString+'</center>';
		data.title = title;
		data.scope = $scope;
		if(withbutton)
			data.buttons = [{text:'OK',type:'button-positive'}]
		popup = $ionicPopup.alert(data); 
}

hideAlert = function($ionicLoading)
{
	$ionicLoading.close();
}
angular.module('starter.controllers', [])
.factory('userData', function() {
  return {
  		isLogin : false,
    	username 	: 'noman',
    	storename : 'noman',
    	userid	: 'noman',
    	userRfid: '',
    	contact_id:'noman',
    	userTempRfid:'',
    	branchid : 0,
    	branchName : '',
		css:{}

  };
})

.controller('SplashCtrl', function($scope,$location,$http,$ionicPopup,$ionicLoading,userData) {
	
	$scope.input = {};
	console.log(userData);
	$scope.goToRegister = function(){
		$location.path('/register');
	};

	$scope.login = function(){
		
		showLoading($ionicLoading);

	 	$http({
          method  : 'POST',
          url     : API_URL+"/api_flash/login",
          data    : serialize($scope.input), //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
          	hideLoading($ionicLoading);
          	if(data.code == 200)
          	{
          		console.log(data.data);
          		userData.isLogin = true;
          		userData.username = data.data.username;
          		userData.storename = data.data.last_name;
          		userData.userid = data.data.user_id;
          		$location.path('/select-branch')
          	}else if(data.code == 403)
          	{
          		showAlert('Error',data.message,$ionicPopup,$scope,true);
          		$scope.input = {};
          	}
          }).error(function(data){
			  hideLoading($ionicLoading);
			  showAlert('Error',data,$ionicPopup,$scope,true);
			$scope.input = {};
          });
     }
})

.controller('SelectBranchCtrl',function($scope,$http,$location,userData,$ionicPopup,$ionicLoading){
	$scope.items = {};
	$scope.branches = {}; 
	if(userData.username == 'noman' || userData.userid == 'noman' || userData.isLogin == false)
		$location.path('/splash');

	showLoading($ionicLoading);
	
	$http({
		method  : 'POST',
		url     : API_URL+"/api_flash/api_get_branch/",
		data    : serialize({id:userData.userid}), //forms user object
		headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
	})
	.success(function(data) {
		hideLoading($ionicLoading);
		if(data.code == 200){
			$scope.items = data.data.store;
			$scope.branches.selected = data.data.store[0].id;
		}else if(data.code == 404)
		{
			showAlert('Error',data.message,$ionicPopup,$scope,withbutton);
			$location.path('/splash');
		}
	}).error(function(data){
		hideLoading($ionicLoading);
		showAlert('Error','Connection Error',$ionicPopup,$scope,withbutton);
		$location.path('/splash');
	});

	$scope.selectBranch = function()
	{
		userData.branchid = $scope.branches.selected;
		for(i=0;i<$scope.items.length;i++)
		{
			if($scope.items[i].id == $scope.branches.selected)
			{
				userData.branchName = $scope.items[i].name;
			}
		}

		console.log(userData);
		$location.path('/idle-tap')
	}
})

.controller('IdleTapCtrl',function($scope, $location,$http,userData,$ionicPopup,$ionicLoading){
	if(userData.username == 'noman' || userData.userid == 'noman' || userData.branchid == 0 || userData.branchName == '')
		$location.path('/splash');
	
	
	$scope.$on("$ionicView.enter", function(event, data){
		$scope.state = "idle";
   		window.removeEventListener("keyup", $scope.onKeypress);
   		$scope.branchesData = {};
		$scope.userData= userData;
		window.addEventListener("keyup", $scope.onKeypress);

		
		$scope.css = {};
		$scope.css.main_color = "#000000;";
		$scope.css.logourl = API_URL+'/assets/images/'+userData.userid+'.png';
		userData.css = $scope.css; 
	});
 

	$scope.onKeypress = function(event){
		event.preventDefault();
	    if (event.keyCode == 13) {
	    	console.log(userData);
	        userData.userRfid = userData.userTempRfid;
	        userData.userTempRfid = '';

	        $scope.tapRfid();
	    }else{
	    	userData.userTempRfid += event.key;
	    }
	}

	$scope.tapRfid = function(){
		showLoading($ionicLoading);
		
		$http({
			method  : 'POST',
			url     : API_URL+'/api_flash/api_cekin',
			data    : serialize({
				branch_id : userData.branchid,
				rfid : userData.userRfid,
				user_id : userData.userid
			}),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
		})
		.success(function(data) {
			console.log(data);		
			hideLoading($ionicLoading);
			if(data.code == 200){
				console.log(data);
				window.removeEventListener("keyup", $scope.onKeypress);
				showAlert('SUCCESS',data.message,$ionicPopup,$scope,true);
				console.log(data.data.contact_id);
				userData.contact_id=data.data.contact_id;
				$location.path('/main-page');
			}else if(data.code == 404)
			{
				window.removeEventListener("keyup", $scope.onKeypress);
				$scope.myPopup = $ionicPopup.show({
					template: '<center><bold> '+data.message+'x </bold></center>',
					title: 'ERROR',
					subTitle: "Tap Rfid",
					scope: $scope,
					buttons:[
			    	{
			    	text:'Cancel',
			    	type:'button-positive',
			    	onTap: function(e) {
						window.addEventListener("keyup", $scope.onKeypress);
					 }
			    	}]

				});
			}
		}).error(function(data){
			window.removeEventListener("keyup", $scope.onKeypress);
			hideLoading($ionicLoading);
		});	
	}
	
	$scope.simulateClick = function(){
		showLoading($ionicLoading);
		
		$http({
			method  : 'POST',
			url     : API_URL+'/api_flash/api_cekin',
			data    : serialize({
				branch_id : userData.branchid,
				rfid : 123123,
				user_id : userData.userid
			}),
			headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
		})
		.success(function(data) {
			console.log(data);		
			hideLoading($ionicLoading);
			if(data.code == 200){
				console.log(data);
				window.removeEventListener("keyup", $scope.onKeypress);
				showAlert('SUCCESS',data.message,$ionicPopup,$scope,true);
				$location.path('/main-page');
			}else if(data.code == 404)
			{
				showAlert('Error',data.message,$ionicPopup,$scope,true);
			}
		}).error(function(data){
			window.removeEventListener("keyup", $scope.onKeypress);
			hideLoading($ionicLoading);
		});
	}
	$scope.registerUser = function(){
		window.removeEventListener("keyup",$scope.onKeypress);
		$location.path('/register');
	}
})

.controller('RegisterCtrl',function($scope, $location,userData,$ionicLoading,$http,$ionicPopup,$filter){
	if(userData.username == 'noman' || userData.userid == 'noman' || userData.isLogin == false || userData.branchName == '')
		$location.path('/splash');

	$scope.$on("$ionicView.enter", function(event, data){
   		window.addEventListener("keyup", $scope.onKeypress);
		$scope.css = userData.css;
		
	});

	$scope.onKeypress = function(event){
		event.preventDefault();
	    if (event.keyCode == 13) {
	    	console.log(userData);
	        userData.userRfid = userData.userTempRfid;
	        userData.userTempRfid = '';
	        $scope.tapRfid();
	    }else{
	    	userData.userTempRfid += event.key;
	    }
	}
	
	$scope.tapRfid = function(){
		
		showLoading($ionicLoading);

		$http({
		method  : 'GET',
		url     : API_URL+'/api_flash/api_cek_id?user_id='+userData.userid+'&rfid='+userData.userRfid+'&glob=1&type=1',
		headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
		})
		.success(function(data) {
			hideLoading($ionicLoading);
			if(data.code == 200){
				showAlert('Error','Kartu sudah pernah digunakan',$ionicPopup,$scope,true);
			}else if(data.code == 404)
			{
				window.removeEventListener("keyup", $scope.onKeypress);
				$location.path('/register-form')
			}
		}).error(function(data){
			window.removeEventListener("keyup", $scope.onKeypress);
			hideLoading($ionicLoading);
		});	
	}

	$scope.registerThisCard = function(){
		window.removeEventListener("keyup", $scope.onKeypress);
		$location.path('/register-form');
	}

	$scope.backToRegister = function(){
		window.removeEventListener("keyup", $scope.onKeypress);	
		$location.path('/idle-tap');
	}
})

.controller('RegisterFormCtrl',function($scope, $location,userData,$ionicLoading,$http,$ionicPopup){
	if(userData.username == 'noman' || userData.userid == 'noman' || userData.isLogin == false || userData.branchName == '' || userData.branchid == undefined)
		$location.path('/splash');

	$scope.$on("$ionicView.enter", function(event, data){
		try{
		if(ionic.Platform.isAndroid())
			Keyboard.show();
		}catch(e){};
   		$scope.formDatas = {};
		$scope.formDatas.city = userData.branchName;
		$scope.formDatas.first_name = "";
		$scope.formDatas.phone_number = "";
		$scope.formDatas.email = "";
		$scope.formDatas.gender_id = "";
	});

	$scope.back = function(){
		$location.path('/idle-tap');
	}

	$scope.submitForm = function(){
		showLoading($ionicLoading);

		$scope.formDatas.date_birth = getDate($scope.formDatas.date);
		$scope.formDatas.city = userData.branchName;
		$scope.formDatas.city_id = userData.branchid;
		$scope.formDatas.user_id = userData.userid;
		$scope.formDatas.rfid = userData.userRfid;

		console.log(serialize($scope.formDatas));

		if($scope.formDatas.first_name == "" || $scope.formDatas.phone_number == "" || $scope.formDatas.email == "" || $scope.formDatas.gender_id == "")
		{
			hideLoading($ionicLoading);
			showAlert('WARNING','Data tidak lengkap',$ionicPopup,$scope,true);

			return;
		}

		$http({
          method  : 'POST',
          url     : API_URL+'/api_flash/api_add_user',
          data    : serialize($scope.formDatas), //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
          	hideLoading($ionicLoading);
          	showAlert('SUCCESS','Register success',$ionicPopup,$scope,true);
          	$location.path('/idle-tap');
          }).error(function(data){
          	hideLoading($ionicLoading);
          });


	}
})

.controller('MainPageCtrl',function($scope,$http,userData, $location,$ionicPopup,$ionicLoading){

	$scope.socialData = {};

	$scope.back = function(){
		//$location.path('/register');
	}
	$scope.visits = {};
	$scope.visits.weekly = 0;
	$scope.visits.monthly = 0;
	$scope.visits.total = 0;
	$scope.main_data = null;

	$scope.doShareFb = function(){
		
		showAlert('WAIT','Comming Soon',$ionicPopup,$scope,true);


		// $cordovaSocialSharing
	 //    .shareViaFacebook(message, API_URL+'/assets/images/'+userData.userid+'.png', $scope.fb_url)
	 //    .then(function(result) {
	 //      	showAlert('SUCCESS','BERHASIL',$ionicPopup,$scope,true);
	 //    }, function(err) {
	 //    	showAlert('ERROR','GAGAL',$ionicPopup,$scope,true);
	 //      // An error occurred. Show a message to the user
	 //    });
	}
	$scope.getWeekly = function()
	{
		return $scope.visits.weekly;
	}
	$scope.getMonthly = function()
	{
		return $scope.visits.monthly;
	}
	$scope.getTotal = function()
	{
		return $scope.visits.total;
	}
	$scope.syncDataUser = function(){
		showLoading($ionicLoading);
		$http({
          method  : 'GET',
          url     : API_URL+'/api_flash/api_get_detail_data?contact_id='+userData.contact_id,
          data    : serialize($scope.formDatas), //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
          	console.log(data);
          	$scope.main_data = data.data; 
          	$scope.main_data.storename = userData.storename;
          	hideLoading($ionicLoading);
          	console.log($scope.main_data);

          	$scope.syncVisit();

          }).error(function(data){
          	showAlert('Error','ERROR',$ionicPopup,$scope,true);
          	$location.path('/idle-tap');
          	hideLoading($ionicLoading);
          });
	};

	$scope.syncVisit = function(){
      	showLoading($ionicLoading);
   		console.log(userData.contact_id);
		$http({
          method  : 'GET',
          url     : API_URL+'/api_flash/api_get_visit?contact_id='+userData.contact_id,
          data    : serialize($scope.formDatas), //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
          	console.log(data);
          	$scope.visits = data.data;
          	hideLoading($ionicLoading);
          	console.log($scope.main_data);
          	$scope.syncActivityBranch();
          }).error(function(data){
          	showAlert('Error','ERROR',$ionicPopup,$scope,true);
          	$location.path('/idle-tap');
          	hideLoading($ionicLoading);
          });
	};
	$scope.syncActivityBranch = function(){
	   $http({
	      method  : 'GET',
	      url     : API_URL+'/api_flash/get_activities?user_id='+userData.userid+"&branch_id="+userData.branchid,
	      data    : serialize($scope.formDatas), //forms user object
	      headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
	     })
	      .success(function(data) {
	      	console.log(data);
	      	$scope.activities = data.data;
	      	console.log($scope.activities);
	      	hideLoading($ionicLoading);
	      }).error(function(data){
	      	showAlert('Error','ERROR',$ionicPopup,$scope,true);
	      	$location.path('/idle-tap');
	      	hideLoading($ionicLoading);
	      });
	}
	$scope.syncReedemBranch = function(){
	   $http({
	      method  : 'GET',
	      url     : API_URL+'/api_flash/api_get_reedem?user_id='+userData.userid+"&branch_id="+userData.branchid,
	      data    : serialize($scope.formDatas), //forms user object
	      headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
	     })
	      .success(function(data) {
	      	console.log(data);
	      	$scope.reedems = data.data;
	      	hideLoading($ionicLoading);
	      }).error(function(data){
	      	showAlert('Error','ERROR',$ionicPopup,$scope,true);
	      	$location.path('/idle-tap');
	      	hideLoading($ionicLoading);
	      });
	}

	$scope.$on("$ionicView.enter", function(event, data){ 
		$scope.syncDataUser();
		$scope.syncReedemBranch();
		$scope.getSocialShare();
		$scope.css = {};
		$scope.css.logourl = API_URL+'/assets/images/'+userData.userid+'.png';


	});

	$scope.getSocialShare = function()
	{
		
		$scope.socialData.user_id = userData.userid;

		$http({
          method  : 'GET',
          url     : API_URL+'/api_flash/get_fb_url/'+userData.userid,
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
          	console.log(data);
			$scope.socialData.fb_url = data.data;         

          }).error(function(data){
          	showAlert('Error','ERROR',$ionicPopup,$scope,true);
          	$location.path('/idle-tap');
          	hideLoading($ionicLoading);
          });
	}
	$scope.getStyle = function(){
        var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

        return {
            'top': $scope.isSemi ? 'auto' : '50%',
            'bottom': $scope.isSemi ? '5%' : 'auto',
            'left': '50%',
            'transform': transform,
            '-moz-transform': transform,
            '-webkit-transform': transform,
            'font-size': $scope.radius/3.5 + 'px'
        };
    };
    
    $scope.doActivity = function(nama,id,point){
    	
    	$scope.myPopup.close();
    	$scope.data = {};
    	$scope.data.nama = nama;
    	$scope.data.activity_id = id;

    	var myPopup = $ionicPopup.show({
	    template: '<input style="text-align:center" type="text" ng-model="data.quantity">',
	    title: nama+' '+point+' point',
	    subTitle: 'QUANTITY',
	    scope: $scope,
	    buttons:[
	    {
	    	text:'Cancel'
	    },
		{
			text: '<b>OK</b>',
			type: 'button-positive',
			onTap: function(e) {
			  if (!$scope.data.quantity) {
			    //don't allow the user to close unless he enters wifi password
			    e.preventDefault();
			  } else {
			  	//showLoading($ionicLoading);
			    $scope.useActivity();
			    return;
			  }
			}
		}
	      ]
	  });
    }

     $scope.doReedem = function(nama,id,point){
    	
    	$scope.myPopup.close();
    	$scope.data = {};
    	$scope.data.activity_id = id;
    	$scope.data.nama = nama;

    	var myPopup = $ionicPopup.show({
	    template: 'Poin kamu akan berkurang '+point,
	    title: 'REEDEM '+nama ,
	    scope: $scope,
	    buttons:[
	    {
	    	text:'Cancel'
	    },
		{
			text: '<b>OK</b>',
			type: 'button-positive',
			onTap: function(e) {
			  	showLoading($ionicLoading);
			    $scope.useReedem();
			    return;
			 }
		}
	    ]
	  });
    }

     $scope.doClaim = function(nama,id,point){
    	
    	$scope.myPopup.close();
    	$scope.data = {};
    	$scope.data.activity_id = id;
    	$scope.data.nama = nama;

    	var myPopup = $ionicPopup.show({
	    template: '<div style="text-align:center">Masukkan kode yang ada di email kamu</div><input style="text-align:center" type="text" ng-model=data.key>',
	    title: 'REEDEM '+nama ,
	    scope: $scope,
	    buttons:[
	    {
	    	text:'Cancel'
	    },
		{
			text: '<b>OK</b>',
			type: 'button-positive',
			onTap: function(e) {
				if(!$scope.data.key){
					e.preventDefault();
					return
				}else{
				  	showLoading($ionicLoading);
				    $scope.useClaim();
				    return;
				}
			 }
		}
	    ]
	  });
    }

    $scope.useActivity = function(){
    	$scope.state = "validateactivity";

    	$scope.postActivity = {};
    	$scope.postActivity.contact_id = userData.contact_id;
    	$scope.postActivity.user_id = userData.userid;
    	$scope.postActivity.branch_id = userData.branchid;
    	$scope.postActivity.activity_id = $scope.data.activity_id;
    	$scope.postActivity.quantity = $scope.data.quantity;
    	
    	window.removeEventListener("keyup", $scope.cekRfidOfficer2);
		window.addEventListener("keyup", $scope.cekRfidOfficer2);
		$scope.tempOficerRfid = "";
		$scope.officerRfid = "";


    	$scope.myPopup = $ionicPopup.show({
			template: '<center><bold> '+$scope.data.nama +' '+$scope.postActivity.quantity+'x </bold></center>',
			title: 'VALIDASI PETUGAS',
			subTitle: "Tap Rfid",
			scope: $scope,
			buttons:[
	    	{
	    	text:'Cancel',
	    	type:'button-positive',
	    	onTap: function(e) {
	    		$scope.state = "cancel";
				window.removeEventListener("keyup", $scope.cekRfidOfficer2);
			 }
	    	}]

		});
	}

	$scope.cekRfidOfficer2 = function(event){
		if($scope.state != "validateactivity")
			return;

		event.preventDefault();
		if (event.keyCode == 13) {
			$scope.officerRfid = $scope.tempOficerRfid;
			$scope.postActivity.officerRfid = $scope.tempOficerRfid;
			$scope.tempOficerRfid = '';
			$scope.useActivity2();
			showLoading($ionicLoading);
		}else{
			
			$scope.tempOficerRfid += event.key;
		}
	}


	$scope.useActivity2 = function(){
		console.log($scope.postActivity);
    	$http({
          method  : 'POST',
          url     : API_URL+'/api_flash/api_activity',
          data    : serialize($scope.postActivity), //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {

          	hideLoading($ionicLoading);
          	if(data.code != 200){
          		showAlert('ERROR',data.message,$ionicPopup,$scope,true);
          	}else{
          		$scope.myPopup.close();	
          		window.removeEventListener("keyup");
          		$scope.state = "validateactivityfinish";
          		$scope.myPopup = $ionicPopup.show({
					template: '<center><bold> '+data.message+' </bold></center>',
					title: 'SUKSES',
					subTitle: "Selamat",
					scope: $scope,
					buttons:[
			    	{
			    	text:'Cancel',
			    	type:'button-positive',
			    	onTap: function(e) {
						window.removeEventListener("keyup", $scope.cekRfidOfficer2);
					 }
			    	}]

				});


          		showAlert('SUCCESS',data.message,$ionicPopup,$scope,true);
          		$scope.myPopup.close();	
          	}
          	$scope.syncDataUser();
          }).error(function(data){
          	$scope.state = "validateerror";
          	showAlert('Error','ERROR',$ionicPopup,$scope,true);
          	$location.path('/idle-tap');
          	hideLoading($ionicLoading);
          });	
    }

    $scope.useReedem = function(){
    	$scope.postActivity = {};
    	$scope.postActivity.contact_id = userData.contact_id;
    	$scope.postActivity.user_id = userData.userid;
    	$scope.postActivity.branch_id = userData.branchid;
    	$scope.postActivity.activity_id = $scope.data.activity_id;
    	$scope.postActivity.quantity = $scope.data.quantity;
    	$http({
          method  : 'POST',
          url     : API_URL+'/api_flash/api_reedem',
          data    : serialize($scope.postActivity), //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
          	hideLoading($ionicLoading);
          	showAlert('SELAMAT',data.message +" "+$scope.data.nama,$ionicPopup,$scope,true);
          	$scope.syncDataUser();
          }).error(function(data){
          	showAlert('Error','ERROR',$ionicPopup,$scope,true);
          	$location.path('/back');
          	hideLoading($ionicLoading);
          });	
    }


    $scope.useClaim = function(){
    	$scope.postActivity = {};
    	$scope.postActivity.contact_id = userData.contact_id;
    	$scope.postActivity.user_id = userData.userid;
    	$scope.postActivity.branch_id = userData.branchid;
    	$scope.postActivity.activity_id = $scope.data.activity_id;
    	$scope.postActivity.key = $scope.data.key;

    	$http({
          method  : 'POST',
          url     : API_URL+'/api_flash/api_claim',
          data    : serialize($scope.postActivity), //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
          	hideLoading($ionicLoading);
			
			$scope.validateReedem();
          	
          }).error(function(data){
          	showAlert('Error','ERROR',$ionicPopup,$scope,true);
          	$location.path('/back');
          	hideLoading($ionicLoading);
          });	
    }

	$scope.validateReedem = function(){
		$scope.state = "validateclaim";
		$scope.myPopup = $ionicPopup.show({
			template: '<center>VALIDASI PETUGAS</center>',
			title: 'SELAMAT',
			subTitle: 'SELAMAT KAMU BERHAK ATAS <bold> '+$scope.data.nama +" </bold> tunjukkan ke petugas untuk verivikasi",
			scope: $scope
		});

		window.removeEventListener("keyup", $scope.cekRfidOfficer);
		window.addEventListener("keyup", $scope.cekRfidOfficer);
		$scope.tempOficerRfid = "";
		$scope.officerRfid = "";
	
	}

	$scope.cekRfidOfficer = function(event){
		event.preventDefault();
		if($scope.state != "validateclaim")
			return;

		if (event.keyCode == 13) {
			$scope.officerRfid = $scope.tempOficerRfid;
			$scope.tempOficerRfid = '';
			$scope.validateOfficer();
			showLoading($ionicLoading);
		}else{
			
			$scope.tempOficerRfid += event.key;
		}
	}

	$scope.validateOfficer = function(){
		$scope.postActivity.officer_rfid = $scope.officerRfid;
		$http({
			method  : 'POST',
			url     : API_URL+'/api_flash/api_validate_claim',
			data    : serialize($scope.postActivity), //forms user object
			headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
		})
		.success(function(data) {
			
			hideLoading($ionicLoading);
			if(data.code == 200)
			{
				$scope.state = "validatefinish";
				window.removeEventListener("keyup");
				$scope.myPopup.close();
				$scope.myPopup = $ionicPopup.show({
					template: '<center><bold> '+data.message+' </bold></center>',
					title: 'SUKSES',
					subTitle: "Selamat",
					scope: $scope,
					buttons:[
			    	{
			    	text:'Cancel',
			    	type:'button-positive',
			    	onTap: function(e) {
						window.removeEventListener("keyup", $scope.cekRfidOfficer);	
					 }
			    	}]

				});
			}else{
				showAlert('ERROR',data.message,$ionicPopup,$scope,true);	
			}
			
		}).error(function(data){
			showAlert('Error','ERROR',$ionicPopup,$scope,true);
			hideLoading($ionicLoading);
		});	
	}

    $scope.showActivity = function(){

    	var tempString = '';
		for(var i=0;i<$scope.activities.length;i++)
		{
			tempString += '<button class="button button-positive" id="select-branch-button-submit" ng-click="doActivity('+"'"+$scope.activities[i].name+"'"+','+$scope.activities[i].id+','+$scope.activities[i].point+')">'+$scope.activities[i].name+'  '+$scope.activities[i].point+'</button>';
		}

	$scope.myPopup = $ionicPopup.show({
	    template: tempString,
	    title: 'ACTIVITY',
	    subTitle: 'Pilih aktivitas yang ingin dilakukan',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel' }
	    ]
	  });
    }
    $scope.showReedem = function(){
    	
    	var tempString = '';
		console.log($scope.reedems);
		
		for(var i=0;i<$scope.reedems.length;i++)
		{
			tempString += '<button class="button button-positive" id="select-branch-button-submit" ng-click="doReedem('+"'"+$scope.reedems[i].name+"'"+','+$scope.reedems[i].id+','+$scope.reedems[i].point+')">'+$scope.reedems[i].name+'  '+$scope.reedems[i].point+'</button>';
		}

		$scope.myPopup = $ionicPopup.show({
	    template: tempString,
	    title: 'REEDEM',
	    subTitle: 'Pilih hadiah yang ingin di reedem',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel' }
	    ]
	  });
    }

    $scope.showClaim = function(){
    	
    	var tempString = '';
		for(var i=0;i<$scope.reedems.length;i++)
		{
			tempString += '<button class="button button-positive" id="select-branch-button-submit" ng-click="doClaim('+"'"+$scope.reedems[i].name+"'"+','+$scope.reedems[i].id+','+$scope.reedems[i].point+')">'+$scope.reedems[i].name+'  '+$scope.reedems[i].point+'</button>';
		}

		$scope.myPopup = $ionicPopup.show({
	    template: tempString,
	    title: 'CLAIM',
	    subTitle: 'Pilih hadiah yang ingin di klaim',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel' }
	    ]
	  });
    }

    $scope.doLogout = function(){
    	$location.path('/idle-tap');
    }
})




